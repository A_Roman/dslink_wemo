import "dart:async";
import "package:upnp/upnp.dart";
import 'package:dslink_wemo/dslink_wemo.dart' as wemo;
import 'package:dslink/dslink.dart';



Future<Map<String,dynamic>> GetServices(Device device) async {

  Service basicService = await device.getService(
      'urn:Belkin:service:basicevent:1');
  Service firmwareService = await device.getService(
      'urn:Belkin:service:firmwareupdate:1');
  Service rulesService = await device.getService(
      'urn:Belkin:service:rules:1');

  Map<String, dynamic> deviceToServicesMap = {
    device.uuid : {
      'basicEvent' : basicService,
      'firmwareEvent' : firmwareService,
      'rules' : rulesService
    }
  };

  return deviceToServicesMap;
}

Subscribe(String uuid, String serviceName, String variableName,
    Map<String, dynamic> deviceSubscriptionsAndTimers, Map<String, dynamic> deviceToServicesMap,
    LinkProvider link) async {

  print('Log :: {Subscribe} Starting $uuid subscription at ${new DateTime.now().toUtc()}');

  StateSubscriptionManager stateSubscriptionManager = new StateSubscriptionManager();

  StateVariable binaryVariable = deviceToServicesMap[uuid][serviceName].stateVariables
      .firstWhere((StateVariable variable) => variable.name == variableName);

  //has 2 in timeout after idling
  await stateSubscriptionManager.init();

  stateSubscriptionManager.subscribeToVariable(binaryVariable).listen((value) {


    print('Log :: {Subscribe} Subscription picked up for $uuid with new state ${value}');
    if ((value == 1 ||  value == 0))
    {
      link.val('/${uuid}/stateValue', value);

      bool postingState;
      try {
        LocalNode lightswitchNode = link.getNode('/$uuid');
        postingState = (lightswitchNode.getAttribute('@posting').toString() == "true" );
      } catch (e) {
        postingState = false;
      }

      if (postingState == true) {
        wemo.HttpPostCallFromPath('/$uuid', 'stateValue=${value}', link);
      }

      link.save();
    }

  }, onError: (e) {
    print('Error :: {Subscribe} while subscribing to ${binaryVariable.name} for ${uuid}');

  });
  deviceSubscriptionsAndTimers[uuid]['subscriber'] = stateSubscriptionManager;

}

Future<dynamic> GetCurrentState(Map<String,dynamic> deviceServices) async {

  Map<String,String> result;
  Service service = deviceServices['basicEvent'];

  result = await service.invokeAction('GetBinaryState', {});

  return result;
}

Future<dynamic> ToggleCurrentState(deviceServices) async {

  Map<String,String> result;
  Service service = deviceServices['basicEvent'];

  Map<String,String> binaryResult = await GetCurrentState(deviceServices);

  int toggleValue;

  if (binaryResult['BinaryState'].toString() == '0') { toggleValue = 1; }
  else { toggleValue = 0; };

//print('Current Result :: ${binaryResult['BinaryState']} to become :: $toggleValue');

//|---- SetBinaryState  is related to state value >BinaryState< has direction {IN}
//|---- Duration  is related to state value >Duration< has direction {IN}
//|---- EndAction  is related to state value >EndAction< has direction {IN}
//|---- UDN  is related to state value >UDN< has direction {IN}
//|---- CountdownEndTime  is related to state value >CountdownEndTime< has direction {OUT}
//|---- deviceCurrentTime  is related to state value >deviceCurrentTime< has direction {OUT}

  result = await service.invokeAction('SetBinaryState', {
    'BinaryState' : toggleValue
  });

  return result;
}

Future<dynamic> GetFirmwareVersion(Map<String,dynamic> deviceServices) async {

  Map<String,String> result;
  Service service = deviceServices['firmwareEvent'];

//|---- FirmwareVersion  is related to state value >FirmwareVersion< has direction {OUT}

  result = await service.invokeAction('GetFirmwareVersion', {});

  return result;

}

Future<dynamic> GetSignalStrength(Map<String,dynamic> deviceServices) async {

  Map<String,String> result;
  Service service = deviceServices['basicEvent'];

//|---- SignalStrength  is related to state value >SignalStrength< has direction {OUT}

  result = await service.invokeAction('GetSignalStrength', {});

  return result;

}

Future<dynamic> GetRulesInfo(Map<String,dynamic> deviceServices) async {

  Map<String,String> result;
  Service service = deviceServices['rules'];

//|---- ruleDbPath  is related to state value >ruleDbPath< has direction {OUT}
//|---- ruleDbVersion  is related to state value >ruleDbVersion< has direction {OUT}
//|---- errorInfo  is related to state value >errorInfo< has direction {OUT}

  result = await service.invokeAction('FetchRules', {});

  return result;

}

Future<dynamic> StoreRules(Map<String,dynamic> deviceServices, String ruleBody, String dbVersion, String proccesDB) async {

  Map<String,String> result;
  Service service = deviceServices['rules'];

//|---- ruleDbVersion  is related to state value >ruleDbVersion< has direction {IN}
//|---- processDb  is related to state value >processDb< has direction {IN}
//|---- ruleDbBody  is related to state value >ruleDbBody< has direction {IN}
//|---- errorInfo  is related to state value >errorInfo< has direction {OUT}

  result = await service.invokeAction('StoreRules',
    {
      'ruleDbVersion' : dbVersion,
      'processDb'     : proccesDB,
      'ruleDbBody'    : ruleBody
    }
  );

  return result;

}

Future<dynamic> FirmwareUpdate(Map<String,dynamic> deviceServices, String newFirmwareVersion, String releaseDate, String url, String signature) async {

  Map<String,String> result;
  Service service = deviceServices['UpdateFirmware '];

//|---- NewFirmwareVersion  is related to state value >NewFirmwareVersion< has direction {IN}
//|---- ReleaseDate  is related to state value >ReleaseDate< has direction {IN}
//|---- URL  is related to state value >URL< has direction {IN}
//|---- Signature  is related to state value >Signature< has direction {IN}
//|---- DownloadStartTime  is related to state value >DownloadStartTime< has direction {IN}
//|---- WithUnsignedImage  is related to state value >WithUnsignedImage< has direction {IN}

  result = await service.invokeAction('StoreRules',
      {
        'NewFirmwareVersion' : newFirmwareVersion,
        'ReleaseDate'        : releaseDate,
        'URL'                : url,
        'Signature'          : signature,
        'DownloadStartTime'  : 0,
        'WithUnsignedImage'  : ''
      }
  );

  return result;

}

Map<String,String> FailedResponse(e) {

  Map<String,String> failedResponse = {
    'ERROR' : 'ERROR - unable to ineract with device'
  };
  return failedResponse;
}