import "package:upnp/upnp.dart";
import "dart:async";

Future<Device> GetDevice(String ip, String port) async {

  print('Log :: {Get Device} Getting device...');
  Device device;

  try {
    DiscoveredClient discoveredClient = new DiscoveredClient();
    discoveredClient.location = "http://${ip}:${port}/setup.xml";
    device = await discoveredClient.getDevice();
  } catch (e) {
    print('Error :: {Get Device} Failed to get device');
//    rethrow;
  }

  return device;
}

Future<Map<String, Device>> GetDevicesList() async {

  DeviceDiscoverer discoverer = new DeviceDiscoverer();

  print('Log :: {Device Discovery} Searching for devices...');
  Map<String, Device> foundLightSwitches = {};

  try {
    await for (DiscoveredClient client in discoverer.quickDiscoverClients(
        timeout: const Duration(seconds: 30)

    )) {

//      print('Found device with name $client');
      String nameSection = client.usn.split(':')[1].split('-')[0];
      print('Log :: {Device Discovery} Found device with name $nameSection');

      Device deviceFound;

      if (nameSection == "Lightswitch") {
        try {
          deviceFound = await client.getDevice();
        } catch (e) {
          continue;
        }
        print("Log :: {Device Discovery} Discovered Lightswitch: ${deviceFound.friendlyName}");

        foundLightSwitches[deviceFound.uuid] = deviceFound;

      }
      continue;

    }
  } catch (e, stack) {
    print("Log :: {Device Discovery} Failed to discover devices : $e , $stack");
  }

  discoverer.stop();

  print('Log :: {Device Discovery} Discoverer closed ');

  return foundLightSwitches;
}
