
bool checkIfValidIP(dynamic ip) {

  bool _isGoodIP = true;

  //if input in empty
  if (ip == null)               { _isGoodIP = false; }
  else if ( ip is !String)      { _isGoodIP = false; }
  else if (!ip.contains('.'))   { _isGoodIP = false; }
  else {

    List<String> _brokenIP = ip.split('.');

    if (_brokenIP.length != 4) { _isGoodIP = false; }
    else {

      _brokenIP.forEach( (_value) {
        int _valueInt;

        if (_isGoodIP) {

          try {
            _valueInt = int.parse(_value);
          } catch (e) {
            _valueInt = 1000;
          }

          if (_isGoodIP == true) {
            if (_valueInt > 999) { _isGoodIP = false; }
          };
        }
      });
    }
  };

  return _isGoodIP;
}

bool checkIfValidPort(dynamic port) {

  bool _isGoodPort = true;

  int _portInt;

  try       { _portInt = int.parse(port); }
  catch (e) { _portInt = 65536; } // change port is wrong

  if (_portInt > 65535)    { _isGoodPort = false; }

  return _isGoodPort;

}
