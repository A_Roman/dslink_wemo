import 'package:http/http.dart' as http;
import 'package:dslink/dslink.dart';
import 'dart:async';

Future HttpPostCall(String postUrl, String userKey, String deviceKey, String data) async {

  var response;

  Map<String,String> headers = {
    'connector' : 'DGBOX2EMCORE',
    'userKey'   : userKey,
    'deviceKey' : deviceKey
  };

  DateTime now = new DateTime.now().toUtc();

  try {
    response = await http.post(Uri.parse(postUrl),headers: headers,body: data);
//        .timeout(const Duration(seconds: 10)).catchError((e) => print('Error :: {httpPost Error} - $e'));

  } catch (originalException) {

    try {
      response = await http.post(Uri.parse(postUrl),headers: headers,body: data);
    } catch (e) {
      print('Error :: {httpPost Error} Failed to post to device');
      print(e);
    }

  }

  if(response == null || response.statusCode != 200) {
    print('Error :: {Posting Error} [${now}] : statusCode [${response.statusCode}] payload [${data}]');
    print('');
    print(response.body);
    print('');
  }

  print(' POSTING LOG ::: $data returned ${response.body} and ${response.statusCode}');

  return response;
}

Future HttpPostCallFromPath(String path, String data, LinkProvider link) async {

  LocalNode switchRootNode = link.getNode(path);

  if (switchRootNode.getAttribute('@posting'))
  {
      HttpPostCall(
          switchRootNode.getAttribute('@postUrl'),
          switchRootNode.getAttribute('@userKey'),
          switchRootNode.getAttribute('@deviceKey'),
          data);
  }
}