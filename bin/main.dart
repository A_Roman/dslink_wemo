import 'package:dslink_wemo/dslink_wemo.dart' as wemo;
import 'package:dslink/dslink.dart';
import 'package:dslink/nodes.dart';
import 'package:upnp/upnp.dart';

import 'dart:async';
import 'package:petitparser/json.dart' as PetitParser;

LinkProvider link;
SimpleNodeProvider provider;

Requester requester;
SimpleNode devicesNode;

Map<String, dynamic> deviceToServicesMap = {};
Map<String, dynamic> deviceSubscriptionsAndTimers = {};

List<String> ports = ["49153", "49154", "49155"];

main(List<String> args) async {

  Map<String, dynamic> nodeProfiles = {
    'addDevice': (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {
      Path parentPath = new Path(path).parent;

      Device device;
      var ip = params['ip'];
      var port = params['port'];

      if(!wemo.checkIfValidIP(ip)){
        return {
          'message'   : 'Invalid IP value!'
        };
      };

      if(!wemo.checkIfValidPort(port)){
        return {
          'message'   : 'Invalid port value!'
        };
      };


      try {
        device = await wemo.GetDevice(ip, port);
      } catch (e) {
        return {
          'message'   : 'Unable to Connect, check Port or IP'
        };
      }

      List<String> existingSwitches = [];
      link.getNode(parentPath.path).children.forEach( (String nodeName, Node node) {
        if(node.get(r'$is') == 'lightswitchRoot') {
          existingSwitches.add(nodeName);
        }
      });

      if (existingSwitches.contains(device.uuid)) {
        return {
          'message'   : 'Device already exist!'
        };
      }

      try{
        await AddDeviceNode(device);
      } catch (e) {
        print('Log :: unable to add device $ip:$port');
        return {
          'message'   : 'Device was NOT added succesfully',
          'deviceId'  : device.uuid
        };
      }


      return {
        'message'   : 'Device was added succesfully',
        'deviceId'  : device.uuid
      };
    }),


    'discoverDevices' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;
      LocalNode parentNode = link.getNode(parentPath.path);
      bool autoDiscovery = parentNode.getAttribute('@autoDiscovery').toString() == 'true';

      Map<String,Device> foundLightSwitches;

      if (autoDiscovery) {

        return {
          'message' : 'Currently searching... Please wait',
        };

      } else {

        parentNode.attributes['@autoDiscovery'] = true;
        parentNode.listChangeController.add('@autoDiscovery');
        foundLightSwitches = await wemo.GetDevicesList().whenComplete( ()
        {
          parentNode.attributes['@autoDiscovery'] = false;
          parentNode.listChangeController.add('@autoDiscovery');
        });

      }

      parentNode.children.values.forEach( (node)
        {
          if (node.get(r'$is') == 'lightswitchRoot') {
            String knownUuid = node.get('@uuid');
            foundLightSwitches.remove(knownUuid);
          }
        }
      );

      if (foundLightSwitches.isNotEmpty) {
        foundLightSwitches.forEach( (uuid, device) {
          AddDeviceNode(device);
        });
      }

      String message = '';

      if (foundLightSwitches.isEmpty) {
        message = "Search finished, failed to find new devices";
      } else {
        message = "Search finished, found : ";
        for (String foundSwitchUUID in foundLightSwitches.keys) {
          message = message + ' [$foundSwitchUUID] ';
        }
      }

      return {
        'message' : message,
      };

    }),

    'changeFriendlyName' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {
      Path parentPath = new Path(path).parent;
      String newFriendlyName = params['newFriendName'].toString();

      print('Log :: Changing friendly name of ${parentPath.path}');
      var result;

      LocalNode lNode = link.getNode(parentPath.path);
      String uuid = lNode.getAttribute('@uuid');
      Service service = deviceToServicesMap[uuid]['basicEvent'];

      try{
        result = await service.invokeAction('ChangeFriendlyName', {
          'FriendlyName' : newFriendlyName
        });
      } catch (originalException) {

        try{
          ReconnectionRequest(uuid);
        } catch (e) {
          print('Error :: {Change Friendly Name} - Error while restablishing failed connection');
          print(originalException);
        }

        return {
          'deviceResponse' : 'Failed to change name!'
        };
      }

      lNode.attributes['@friendlyName'] = newFriendlyName;
      lNode.listChangeController.add('@friendlyName');

      link.saveAsync();

      return{
        'deviceResponse' : result
      };

    }),

    'getState' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {
      Path parentPath = new Path(path).parent;

      print('Log :: Getting current state of ${parentPath.path}');

      LocalNode lNode = link.getNode(parentPath.path);
      String uuid = lNode.getAttribute('@uuid');

      Map<String,String> getStateResult;

      try{
        getStateResult = await wemo.GetCurrentState(deviceToServicesMap[uuid]);
      } catch (originalException) {

        try{
          ReconnectionRequest(uuid);
        } catch (e) {
          print('Error :: {Get Switch State} - Error while restablishing failed connection');
          print(originalException);
        }

      }

      return{
        'deviceResponse' : getStateResult
      };

    }),

    'storeCustomEvents' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;


//      PetitParser.JsonParser jsonParser = new PetitParser.JsonParser();

      bool response = false;
//      Map<String,dynamic> weeklySchedule = link.val('$uuid/schedule');
//      Map<String,dynamic> currentCustomEvents = link.val('$uuid/customEvents');

      var customEventsIn = params['customEvents'];

//      if (customEventsIn is String) {
//        customEventsIn = jsonParser.parse(customEventsIn);
//      }

      // this should cast the Map
//      customEventsIn = customEventsIn as Map<String,dynamic>;

      // this will replace the events if the issue if no issues
      link.updateValue('${parentPath.path}/customEvents',customEventsIn);

      //TODO: need to add method to nteract with emcorr if new schedul is needed

      link.saveAsync();
      response = true;

      return{
        'succeeded' : response
      };

    }),

    'getSignalStrength' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {
      Path parentPath = new Path(path).parent;

      print('Log :: getting current signal strength of ${parentPath.path}');

      LocalNode lNode = link.getNode(parentPath.path);
      String uuid = lNode.getAttribute('@uuid');
      var getSignalStrengthResult;

      try{
        getSignalStrengthResult = await wemo.GetSignalStrength(deviceToServicesMap[uuid]);
      } catch (originalException) {

        try{
          ReconnectionRequest(uuid);
        } catch (e) {
          print('Error :: {Get Signal Strength} - Error while restablishing failed connection');
          print(originalException);
        }

      }

      return{
        'deviceResponse' : getSignalStrengthResult
      };

    }),

    'firmwareUpdate' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params)async {

      Path parentPath = new Path(path).parent;
      LocalNode lNode = link.getNode(parentPath.path);
      String uuid = lNode.getAttribute('@uuid');

      String newFirmwareVersion = params["newFirmwareVersion"];
      String releaseDate = params["releaseDate"];
      String url = params["URL"];
      String signature = params["signature"];


      print('Log :: Psuhing a new firmware update for ${parentPath.path}...');

      var firmwareUpdateResult;

      try{
        firmwareUpdateResult = await wemo.FirmwareUpdate(deviceToServicesMap[uuid],
            newFirmwareVersion, releaseDate, url, signature);
      } catch (e) {
        ReconnectionRequest(uuid);
        rethrow;
      }

      link.saveAsync();

      return{
        'deviceResponse' : firmwareUpdateResult
      };

    }),


    'storeSchedule' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params)async {

      Path parentPath = new Path(path).parent;

      var newSchedule = params["newSchedule"];

      String base64db = params["base64db"];

      int overrideTime = int.parse(params['overrideTime'].toString());

      print('Log :: Setting a new schedule for ${parentPath.path}...');

      LocalNode lNode = link.getNode(parentPath.path);
      String uuid = lNode.getAttribute('@uuid');

      Map<String, String> ruleDbInfo = null;

      try{
        ruleDbInfo = await wemo.GetRulesInfo(deviceToServicesMap[uuid]);
      } catch (e) {
        ReconnectionRequest(uuid);
      } finally {

      }

      int ruleDbVersion = 1 + int.parse(ruleDbInfo['ruleDbVersion']);
      String ruleBody = "&lt;![CDATA[$base64db]]&gt;";

      Map<String, String> storeScheduleResult = null;

      try{
        storeScheduleResult = await wemo.StoreRules(deviceToServicesMap[uuid],
            ruleBody, ruleDbVersion.toString(), '1');
      } catch (e) {
        ReconnectionRequest(uuid);
      } finally {

      }

      provider.updateValue('${parentPath.path}/schedule',newSchedule);
      provider.updateValue('${parentPath.path}/overrideTime',overrideTime);

      lNode.attributes['@dbVersion'] = ruleDbVersion;
      lNode.listChangeController.add('@dbVersion');

      lNode.attributes['@lastDBPush'] = new DateTime.now().toUtc().toIso8601String();
      lNode.listChangeController.add('@lastDBPush');

      link.saveAsync();

      bool response = false;

      if (storeScheduleResult.values.first == "Storing of rules DB Successful!\n") {
        response = true;
      }

      return{
        'succeeded' : response
      };

    }),

    'updateSchedule' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params)async {

      Path parentPath = new Path(path).parent;

      var newSchedule = params["newSchedule"];
      int overrideTime = int.parse(params['overrideTime'].toString());

      print('Log :: Updating schedule for ${parentPath.path}...');

      LocalNode lNode = link.getNode(parentPath.path);
      String uuid = lNode.getAttribute('@uuid');

      Map<String, String> ruleDbInfo = null;

      bool establishedConn = false;

      try{
        ruleDbInfo = await wemo.GetRulesInfo(deviceToServicesMap[uuid]);
        establishedConn = true;
      } catch (e) {
        ReconnectionRequest(uuid);
      } finally {

      }

      if (establishedConn == false) {

        return{
          'response' : "Failed to update the schedule, GetRules failed."
        };

      } else {
        provider.updateValue('${parentPath.path}/schedule',newSchedule);
        provider.updateValue('${parentPath.path}/overrideTime',overrideTime);

        lNode.attributes['@dbVersion'] = int.parse(ruleDbInfo['ruleDbVersion']);
        lNode.listChangeController.add('@dbVersion');

        link.saveAsync();
      }

      return{
        'response' : "Successfully updated the schedule!"
      };

    }),

    'togglePosting' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;
      LocalNode lNode = link.getNode(parentPath.path);

      lNode.attributes['@posting'] = lNode.getAttribute('@posting').toString() != "true";

      lNode.listChangeController.add('@posting');
      link.save();

    }),

    'toggleSubscription' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      Path parentPath = new Path(path).parent;
      LocalNode lNode = link.getNode(parentPath.path);
      String uuid = lNode.getAttribute('@uuid');
      bool subscriptionState = lNode.getAttribute('@subscription');
      int postingRate = lNode.getAttribute('@postingRate');


      if (!subscriptionState) {

        deviceSubscriptionsAndTimers[uuid] = {
          'subscriber' : null,
          'subscriberTimer' : null,
          'signalTimer' : null
        };

        wemo.Subscribe(uuid, 'basicEvent', 'BinaryState',
            deviceSubscriptionsAndTimers, deviceToServicesMap, link);
        SignalTimer(uuid, parentPath.path, link, postingRate);
        RefreshTimer(uuid, parentPath.path, link, postingRate);

      } else {

        UnSubscribeToDevices(uuid);

      }

      lNode.attributes['@subscription'] = !subscriptionState;
      lNode.listChangeController.add('@subscription');

      print("Log :: {toggling subscriptions} changed subscription to ${!subscriptionState}");

      link.save();

    }),

    'updatePostingRate' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {
      var nodePath = new Path(path).parentPath;
      LocalNode lNode = provider.getNode(nodePath);

      int newRate = int.parse(params['newRateInMin']);
      bool subscription = (lNode.getAttribute('@subscription').toString() == 'true');
      String uuid = lNode.getAttribute('@uuid');

      print("Log :: {updating posting rate} for $uuid");

      lNode.attributes["@postingRate"] = newRate;

      UnSubscribeToDevices(lNode.getAttribute('@uuid'));

      SubscribeToDevice( subscription, uuid, newRate );

      lNode.listChangeController.add('@postingRate');

      link.save();

    }),

    'publisherSettings' : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) async {

      var nodePath = new Path(path).parentPath;
      var lNode = provider.getNode(nodePath);

      print("Log :: {publiser Settings} changed publisher settings");

      params.forEach((attributeName,newStringValue) {

        params[attributeName].toString().toLowerCase();
        lNode.attributes['@$attributeName'] = newStringValue;
        lNode.listChangeController.add('@$attributeName');

      });

      var deviceConnectionStatus = link.val('${nodePath}/deviceConnectionStatus').toString();
      var signalStrength = link.val('${nodePath}/signalStrength').toString();
      var stateValue = link.val('${nodePath}/stateValue').toString();

      lNode.attributes['@posting'] = true;
      lNode.listChangeController.add('@posting');

      try{
        await wemo.HttpPostCallFromPath(nodePath,'deviceConnectionStatus=$deviceConnectionStatus',link);
        await wemo.HttpPostCallFromPath(nodePath,'signalStrength=$signalStrength',link);
        await wemo.HttpPostCallFromPath(nodePath,'stateValue=$stateValue',link);
        print('Log :: {Publisher Settings} - pushed initial values');
      } catch(e) {
        print('Error :: {Publisher Settings} - Failed to execute initial publisher');
        print(e);
        return{
          'response' : 'Failed! revise connection to device/network'
        };
      }

      link.save();

      return{
        'response' : 'publisher settings changed! '
      };

    }),

//    'removePublisherSettings' : (String path)
//    => new SimpleActionNode(path, (Map<String, dynamic> params) {
//      var parentPath = new Path(path).parent;
//
//      LocalNode lNode = link.getNode(parentPath.path);
//      String uuid = lNode.getAttribute('@uuid');
//
//      print('Log :: Removi');
//
//    }),

    'remove'  : (String path)
    => new SimpleActionNode(path, (Map<String, dynamic> params) {
      var parentPath = new Path(path).parent;

      LocalNode lNode = link.getNode(parentPath.path);
      String uuid = lNode.getAttribute('@uuid');

      print('Log :: Removing ${parentPath.path}');

      //stop subs and refresh timer if subscription is true
      UnSubscribeToDevices(uuid);

      //remove the device from trees
      try{
        deviceToServicesMap.remove('$uuid');
      } catch (e) {
        print("Error :: {remove} removing $uuid from Service Map");
        link.removeNode(parentPath.path);
      }

      try{
        deviceSubscriptionsAndTimers.remove('$uuid');
      }
      catch(e){
        print("Error :: {remove} removing $uuid from subscription and timers");
        link.removeNode(parentPath.path);
      }

      link.removeNode(parentPath.path);

      link.save();
    })
  };

  Map<String, dynamic> defNodes = {
    r'$name'            : 'WeMo DSLink Root',
    r'$is'              : 'DSLink',

    '@autoDiscovery'    : false,

    'discoverDevices'  : {
      r'$name'          : 'Discover Devices',
      r'$invokable'     : 'config',
      r'$is'            : 'discoverDevices',
      r'$result'        : 'values',
      r'$columns'       : [
        {
        'name'    : 'message',
        'type'    : 'string'
        }
      ]
    },

    'addDevice'        : {
      r'$name'          : 'Add Device',
      r'$invokable'     : 'config',
      r'$result'        : 'values',
      r'$params'        : [
        {
          'name'    : 'ip',
          'type'    : 'string',
          'default' : '192.168.1.1'
        },
        {
          'name'    : 'port',
          'type'    : 'string',
          'default' : '49153'
        }
      ],
      r'$columns'       : [
        {
          'name'    : 'message',
          'type'    : 'string'
        },
        {
          "name"    : "deviceId",
          "type"    : "string"
        }
      ],
      r'$is'            : 'addDevice'
    }

  };

  link = new LinkProvider(args
    , "WeMo_LightSwitch-"
    , isResponder: true
    , isRequester: true
    , encodePrettyJson: true
    , defaultNodes: defNodes
    , profiles: nodeProfiles
  );

  provider = link.provider;
  requester = link.requester;

  link.init();

  link.connect().whenComplete(() async {
    RebootConnections();
  }
  );
}

AddDeviceNode(Device device) async {
  String friendlyName = device.friendlyName;

  Map<String, dynamic> newServices = await wemo.GetServices(device);
  deviceToServicesMap.addAll(newServices);

  List<String> address = device.urlBase.split('/')[2].split(':');
  String ip = address[0];
  String port = address[1];

  Map<String, String> binaryResult;
  Map<String, String> firmwareResult;
  Map<String, String> signalResult;
  Map<String, String> dbVersion;

  try {
    binaryResult = await wemo.GetCurrentState(deviceToServicesMap[device.uuid]);
    firmwareResult =  await wemo.GetFirmwareVersion(deviceToServicesMap[device.uuid]);
    signalResult = await wemo.GetSignalStrength(deviceToServicesMap[device.uuid]);
    dbVersion = await wemo.GetRulesInfo(deviceToServicesMap[device.uuid]);
  }catch(e) {
    return;
  }

  int versionNumber = GetProperFirmwareVersion(firmwareResult['FirmwareVersion']);

  // building node

  Map<String, dynamic> addDeviceNodes = {
    r'$name'        : device.uuid,
    r'$is'          : 'lightswitchRoot',
    '@firmwareVer'  : firmwareResult['FirmwareVersion'],
    '@uuid'         : device.uuid,
    '@ipAddress'    : ip,
    '@port'         : port,
    '@friendlyName' : friendlyName,
    '@dbVersion'    : dbVersion['ruleDbVersion'],
    '@lastDBPush'   : 'never',
    '@subscription' : true,
    '@posting'      : false,
    '@userKey'      : 'user-Key',
    '@deviceKey'    : 'device-Key',
    '@postUrl'      : 'https://dgbox2emcore.azurewebsites.net/api/dgconnect',
    '@postingRate'  : 10
  };

  link.addNode('/${device.uuid}', addDeviceNodes);

  //////////
  //VALUES//
  //////////
  link.addNode('/${device.uuid}/stateValue' , {
    r'$name' : 'Switch State',
    r'$type' : 'bool',
    '?value' : binaryResult['BinaryState'],
    r'$is' : 'switchState',
  });

  link.addNode('/${device.uuid}/firmwareVersion' , {
    r'$name' : 'Firmware Version',
    r'$type' : 'string',
    '?value' : versionNumber,
    r'$is' : 'firmwareVersion',
  });

  link.addNode('/${device.uuid}/schedule' , {   //tied within schedule
    r'$name' : 'Schedule',
    r'$type' : 'map',
    '?value' : null,
    r'$is' : 'schedule',
  });

    link.addNode('/${device.uuid}/signalStrength' , {  //tied within schedule
    r'$name' : 'Signal Strength',
    r'$type' : 'num',
    '?value' : int.parse(signalResult['SignalStrength']),
    r'$is' : 'signalStrength',
  });

  link.addNode('/${device.uuid}/deviceConnectionStatus' , {
    r'$name' : 'Device Connection Status',
    r'$type' : 'num',
    '?value' : 1,
    r'$is' : 'deviceConnectionStatus',
  });

  link.addNode('/${device.uuid}/overrideTime' , {
    r'$name' : 'Override Time',
    r'$type' : 'num',
    '?value' : null,
    r'$is' : 'overrideTime',
  });

  link.addNode('/${device.uuid}/customEvents' , {  //tied within schedule
    r'$name' : 'Signal Strength',
    r'$type' : 'num',
    '?value' : int.parse(signalResult['SignalStrength']),
    r'$is' : 'signalStrength',
  });

  link.addNode('/${device.uuid}/customEvents' , {
    r'$name' : 'Custom Events',
    r'$type' : 'map',
    '?value' : {
      "andreDay": {
        "startDate"   :"2017-10-14T09:00:00",
        "endDate"     :"2017-10-14T17:00:00",
        "stateValue"  :'ON'
      },
      "yucelDay": {
        "startDate"   :"2017-03-21T09:00:00",
        "endDate"     :"2017-03-21T17:00:00",
        "stateValue"  :'OFF'
      }
    },
    r'$is' : 'customEvents'
  });

  ///////////
  //ACTIONS//
  ///////////
  link.addNode('/${device.uuid}/changeFriendlyName' , {
    r'$name' : 'Change Friendly Name',
    r'$invokable' : 'write',
    r'$results' : 'values',
    r'$columns' : [
      {
        'name' : 'deviceResponse',
        'type' : 'string'
      }
    ],
    r'$params' : [
      {
        'name': 'newFriendName',
        'type': 'string'
      }
    ],
    r'$is' : 'changeFriendlyName'
  });

  link.addNode('/${device.uuid}/getState' , {
    r'$name' : 'Get Current State',
    r'$invokable' : 'write',
    r'$results' : 'values',
    r'$columns' : [
      {
        'name' : 'deviceResponse',
        'type' : 'string'
      }
    ],
    r'$is' : 'getState'
  });

  link.addNode('/${device.uuid}/firmwareUpdate' ,{
    r'$name' : 'Update Firmware',
    r'$invokable' : 'write',
    r'$results' : 'values',

    r'$params' : [
      {
        'name': 'newFirmwareVersion',
        'type': 'string'
      }
      ,{
        'name': 'releaseData',
        'type': 'string'
      }
      ,{
        'name': 'URL',
        'type': 'string'
      }
      ,{
        'name': 'signature',
        'type': 'string'
      }
    ],

    r'$columns' : [
      {
        'name': 'deviceResponse',
        'type': 'string'
      }
    ],

    r'$is' : 'firmwareUpdate'
  });

  link.addNode('/${device.uuid}/storeSchedule' , {
    r'$name' : 'Store Schedule',
    r'$invokable' : 'write',
    r'$results' : 'values',
    r'$params' : [
      {
        'name': 'newSchedule',
        'type': 'map'
      }
      ,{
        'name': 'overrideTime',
        'type': 'num'
      }
      ,{
        'name': 'base64db',
        'type': 'string'
      }
    ],
    r'$columns' : [
      {
        'name' : 'succeeded',
        'type' : 'bool'
      }
    ],
    r'$is' : 'storeSchedule'
  });

  link.addNode('/${device.uuid}/updateSchedule' , {
    r'$name' : 'Update Schedule',
    r'$invokable' : 'write',
    r'$results' : 'values',
    r'$params' : [
      {
        'name': 'newSchedule',
        'type': 'map'
      }
      ,{
        'name': 'overrideTime',
        'type': 'num'
      }
    ],
    r'$columns' : [
      {
        'name' : 'response',
        'type' : 'string'
      }
    ],
    r'$is' : 'updateSchedule'
  });

  link.addNode('/${device.uuid}/getSignalStrength' , {
    r'$name' : 'Get Signal Strength',
    r'$invokable' : 'write',
    r'$results' : 'values',
    r'$columns' : [
      {
        'name' : 'deviceResponse',
        'type' : 'string'
      }
    ],
    r'$is' : 'getSignalStrength'
  });

  link.addNode('/${device.uuid}/toggleSubscription' , {
    r'$name' : 'Toggle Subscription',
    r'$invokable' : 'write',
    r'$is' : 'toggleSubscription'
  });


  link.addNode('/${device.uuid}/togglePosting' , {
    r'$name' : 'Toggle Posting',
    r'$invokable' : 'write',
    r'$is' : 'togglePosting'
  });

  link.addNode('/${device.uuid}/updatePostingRate' , {
    r'$name' : 'Change Posting Rate',
    r'$invokable' : 'write',
    r'$is' : 'updatePostingRate',
    r'$params' : [
      {
        'name' : 'newRateInMin',
        'type' : 'num'
      }
    ],
  });

  link.addNode('/${device.uuid}/publisherSettings' ,{
    r'$name' : 'Add Publisher Settings',
    r'$invokable' : 'write',
    r'$result' : 'values',
    r'$columns' : [
      {
        'name' : 'response',
        'type' : 'string'
      }
    ],
    r'$params' : [
      {
        'name' : 'userKey',
        'type' : 'string'
      },{
        'name' : 'deviceKey',
        'type' : 'string'
      }
    ],
    r'$is' : 'publisherSettings',
  });

  link.addNode('/${device.uuid}/storeCustomEvents', {
    r'$name' : 'Store Custom Events',
    r'$invokable' : 'write',

    r'$params' : [
      {
        'name': 'customEvents',
        'type': 'map'
      }
    ],

    r'$results' : 'values',
    r'$columns' : [
      {
        'name' : 'succeeded',
        'type' : 'bool'
      }
    ],

    r'$is' : 'storeCustomEvents'
  });

  link.addNode('/${device.uuid}/remove' , {
    r'$name' : 'Remove Device',
    r'$invokable' : 'write',
    r'$is' : 'remove'
  });

  link.save();

  print('Log :: device added {${device.uuid}}');

  SubscribeToDevice(true, device.uuid, 10);

}

RebootConnections() async {

  LocalNode rootNode = ~link;
  Iterable<Node> listChildren = rootNode.children.values;

  String ip;
  String port;
  bool subscription;
  bool posting;
  int postingRate;

  for (Node node in listChildren) {
    if (node.get(r'$is') == 'lightswitchRoot') {

      String uuid = node.get(r'$name');

      print('Log :: {Reboot Conns} Rebooting connections to ${node.get(r'$name')}');
      // get attributes we care about
      ip = node.getAttribute('@ipAddress');
      port = node.getAttribute('@port');
      subscription = node.getAttribute('@subscription').toString() == 'true';
      posting = node.getAttribute('@posting').toString() == 'true';
      postingRate = node.getAttribute('@postingRate');

      Device device;

      //Get the device
      try{
        device = await wemo.GetDevice(ip,port);
      } catch (e) {
        print('Error :: {Reboot Connections} Failed to get $uuid device');
        FailedConnectionUpdate(uuid,e);
        return;
      }

      //Get te services
      try{
        await GetServicesAndAddToMap(device);
      } catch (e) {
        print('Error :: {Reboot Connections} Failed to get $uuid services');
        FailedConnectionUpdate(uuid, e);
        return;
      }

      //subscribe to Services
      try{
        await SubscribeToDevice(subscription, uuid, postingRate);
      } catch (e) {
        print('Error :: {Reboot Connections} Unable to initialize $uuid subscriber connections');
        FailedConnectionUpdate(uuid, e);
        return;
      }

      // this is for getting firmware (this are optional on boot)
      try{
        Map<String, String> firmwareResult = await wemo.GetFirmwareVersion(deviceToServicesMap[device.uuid]);
        int firmwareVersion = GetProperFirmwareVersion(firmwareResult['FirmwareVersion']);
        link.val('/${device.uuid}/firmwareVersion',firmwareVersion);
      } catch (e) {
        print('Error :: {Reboot Connections} Failed to fetch $uuid firmware version');
        print(e);
        continue;
      }

      // this is for getting rulesDbVersion
      try{
        Map<String, String> getRulesInfoResult = await wemo.GetRulesInfo(deviceToServicesMap[device.uuid]);
        node.attributes['@dbVersion'] = int.parse(getRulesInfoResult['ruleDbVersion']);
        link.getNode('/${device.uuid}').listChangeController.add('@dbVersion');
      } catch (e) {
        print('Error :: {Reboot Connections} Failed to fetch $uuid db version');
        print(e);
        continue;
      }

      link.val('/$uuid/deviceConnectionStatus', 1);

      if (posting) {
        try{
          await wemo.HttpPostCallFromPath('/$uuid',"deviceConnectionStatus=1",link);
        } catch (e) {
          print('Error :: {Reconnection Request} Failed to post new connection status');
          print(e);
        }
      }

      print('Log :: {Reboot Connections} $uuid rebooted successfully');

      link.save();

    }
  }
}

SubscribeToDevice(bool subscription, String uuid, int postingRateInMin){

  deviceSubscriptionsAndTimers[uuid] = {
    'subscriber' : null,
    'subscriberTimer' : null,
    'signalTimer' : null
  };

  if (subscription) {
    // this is for refreshTimer
    wemo.Subscribe(uuid, 'basicEvent', 'BinaryState',
        deviceSubscriptionsAndTimers, deviceToServicesMap, link);

    RefreshTimer(uuid, '/$uuid', link, postingRateInMin);

    //this is for signal timer
    SignalTimer(uuid, '/${uuid}', link, postingRateInMin);
  }
}

UnSubscribeToDevices(String uuid) {

  LocalNode lNode = link.getNode('/$uuid');
  bool subscription = lNode.getAttribute('@subscription');

  if (subscription && deviceSubscriptionsAndTimers.containsKey(uuid)) {
    try{
      deviceSubscriptionsAndTimers[uuid]['subscriber'].close();
    } catch(e){
      print("Error :: {Unsubscribe To Devices} failed to close $uuid subscribers");
    }

    try{
      deviceSubscriptionsAndTimers[uuid]['signalTimer'].cancel();
    } catch(e){
      print("Error :: {Unsubscribe To Devices} failed to close $uuid signal timer");
    }

    try{
      deviceSubscriptionsAndTimers[uuid]['subscriberTimer'].cancel();
    } catch(e){
      print("Error :: {Unsubscribe To Devices} failed to close $uuid subscription timer");
    }
  }
}


SignalTimer(String uuid, String path, LinkProvider link, int postingRateInMin) async {

  print('Log :: {Signal Timer} Initializing signal timer for $uuid');
  var duration = new Duration(minutes: postingRateInMin);

  Timer deviceTimer;
  deviceTimer = new Timer.periodic(duration, (_) async {
    print('Log :: {Signal Timer} Starting timer for $uuid');

    // get state of posting variable
    bool postingState;
    try {
      postingState = await WillItPost( uuid, link);
    } catch (e) {
      print('Log :: {Signal Timer} Failed to get postingState, defaulted to FALSE');
      postingState = false;
    }

    try{
      var signalResult = await wemo.GetSignalStrength(deviceToServicesMap[uuid]);

      link.val('$path/signalStrength', int.parse(signalResult['SignalStrength']));

      if (postingState == true) {
        await wemo.HttpPostCallFromPath(path, 'signalStrength=${signalResult['SignalStrength']}', link);
      }

      link.saveAsync();

    } catch (e) {
      print('Error :: {Signal Timer} failed to get signal update');
      ReconnectionRequest(uuid);
    }

  });

  deviceSubscriptionsAndTimers[uuid]['signalTimer'] = deviceTimer;
}

RefreshTimer(String uuid, String path, LinkProvider link, int postingRateInMin) async {
  Timer refreshTimer;
  var duration = new Duration(minutes: postingRateInMin);

  refreshTimer = new Timer.periodic(duration, (_) async {
    print('Log :: {Refresh Timer} Refreshing subscription');

    deviceSubscriptionsAndTimers[uuid]['subscriber'].close();
    deviceSubscriptionsAndTimers[uuid].remove('subscriber');
    try {
      wemo.Subscribe(uuid, 'basicEvent', 'BinaryState',
          deviceSubscriptionsAndTimers, deviceToServicesMap, link);
    } catch (e) {
      print('Log :: {Refresh Timer} refresher failed to start on timer');
      ReconnectionRequest(uuid);
    }
  });

  deviceSubscriptionsAndTimers[uuid]['subscriberTimer'] = refreshTimer;
}

GetServicesAndAddToMap(Device device) async {
  Map<String, dynamic> newServices = await wemo.GetServices(device);
  deviceToServicesMap.addAll(newServices);
  return device;
}

Future<bool> ReconnectionRequest(String uuid) async{

  bool finished = false;

  print('Log :: {Reconnection} Attempting to retablish connection with $uuid...');
  LocalNode lNodeOfReconnectingDevice = link.getNode('/$uuid');

  String ip;
  String port;
  bool subscription;
  bool posting;
  int postingRate;

  try{

    ip = lNodeOfReconnectingDevice.getAttribute('@ipAddress');
    port = lNodeOfReconnectingDevice.getAttribute('@port');
    subscription = lNodeOfReconnectingDevice.getAttribute('@subscription').toString() == 'true';
    posting = lNodeOfReconnectingDevice.getAttribute('@posting').toString() == 'true';
    postingRate = lNodeOfReconnectingDevice.getAttribute('@postingRate');

  } catch (e) {

    return false;

  }

  Device device;

  try{
    device = await wemo.GetDevice(ip, port);
  } catch (e) {
    print('Error :: {Reconnection Request} Failed to get $uuid device');
    FailedConnectionUpdate(uuid, e);
    return finished;
  }

  try{
    GetServicesAndAddToMap(device);
  } catch(e){
    print('Error :: {Reconnection Request} Failed to get $uuid services');
    FailedConnectionUpdate(uuid, e);
    return finished;
  }

  try{
    SubscribeToDevice(subscription, uuid , postingRate);
  } catch(e){
    print('Error :: {Reconnection Request} Failed to subscribe to $uuid ');
    FailedConnectionUpdate(uuid, e);
    return finished;
  }

  link.val('/$uuid/deviceConnectionStatus', 1);

  if (posting) {
    try{
      await wemo.HttpPostCallFromPath('/$uuid',"deviceConnectionStatus=1",link);
    } catch (e) {
      print('Error :: {Reconnection Request} Failed to post new connection status');
      print(e);
    }
  }

  finished = true;
  return finished;
}

FailedConnectionUpdate(String uuid, dynamic exception) async {
  print('');
  print(exception);
  print('');
  UnSubscribeToDevices(uuid);
  link.val('/$uuid/deviceConnectionStatus', 0);

  LocalNode lNodeOfReconnectingDevice = link.getNode('/$uuid');
  bool posting = lNodeOfReconnectingDevice.getAttribute('@posting').toString() == 'true';


  if (posting) {
    try{
      await wemo.HttpPostCallFromPath('/$uuid',"deviceConnectionStatus=1",link);
    } catch (e) {
      print('Error :: {Reconnection Request} Failed to post new connection status');
      print('');
      print(e);
      print('');
    }
  }
  link.save();
}

Future <bool> WillItPost(String uuid, LinkProvider link) async {

  LocalNode lightswitchNode = link.getNode('/$uuid');
  bool postingState = (lightswitchNode.getAttribute('@posting').toString() == "true" );
  return postingState;
}

Future<int> PostingRate(String uuid, LinkProvider link) async {
  LocalNode lightswitchNode = link.getNode('/$uuid');
  int postingRate = int.parse(lightswitchNode.getAttribute('@postingRate'));
  return postingRate;
}

int GetProperFirmwareVersion(String versionResponse) {

//  FirmwareVersion:WeMo_WW_2.00.10885.PVT-OWRT-LS|SkuNo:Plugin Device
  List<String> versionNumbers = versionResponse.split(':')[1].split('|')[0].split('_')[2].split('-')[0].split('.').sublist(0,3);
  List<int> properVersionNumber = [
        (int.parse(versionNumbers[0])) ,
        (int.parse(versionNumbers[1])) ,
        (int.parse(versionNumbers[2]))
      ];

  int version = int.parse(versionNumbers.join(""));
  return version;
}